# FoXML

A single file libary for encoding using a base 64 in Fortran.
This lib is largely adapted from (BeFoR64)[https://github.com/Fortran-FOSS-Programmers/FoXy]


## Requirements
* (PorPre)[https://gitlab.com/RomainNoel/porpre] a portable precision library
* (FoST)[https://gitlab.com/RomainNoel/fost] a string manipulation library
